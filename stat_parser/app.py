import datetime
import json
from datetime import date
from time import sleep
from typing import List
from typing import Tuple

import logging
import re
import dateutil.parser
import html2text
import mysql.connector
import requests
import xmltodict
import pandas as pd

from urllib.parse import unquote
from bs4 import BeautifulSoup
from flask import Flask, request
from mysql.connector import Error, pooling
from tqdm import tqdm
from common_lib import config
from common_lib.TqdmToLogger import TqdmToLogger
h = html2text.HTML2Text()
h.body_width = 0
h.ignore_links = True
CONFIG = config.get_config('./common_lib/config/config.yml')

users = {}

app = Flask(__name__)
username = CONFIG['parser']['username']
password = CONFIG['parser']['password']
session_id = ''
connection_pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="stat_parser_pool",
                                                              pool_size=5,
                                                              user=CONFIG['db']['user'],
                                                              password=CONFIG['db']['password'],
                                                              host=CONFIG['db']['host'],
                                                              database=CONFIG['db']['database'])


def mysql_worker(query, select=False):
    """
    Выполняет команды БД и возвращает результат
    Args:
        query: SQL строка для выполнения
        select: Если True - возвращает результат выполнения операции

    Returns:
        Результат выполнения операции
    """
    result = []
    try:
        connection_object = connection_pool.get_connection()
        if connection_object.is_connected():
            cursor = connection_object.cursor()
            cursor.execute(query)
            if select:
                result = cursor.fetchall()
            connection_object.commit()
    except Error as e:
        app.logger.info("Error while connecting to MySQL using Connection pool ", e)
        app.logger.info(query)
    finally:
        if (connection_object.is_connected()):
            cursor.close()
            connection_object.close()
    return result


def pm_login():
    """
    Авторизация из под пользователя в PM

    Returns:
        Идентификатор сессии для запросов
    """
    session = requests.Session()
    response = session.get('https://pm.is74.ru/login')
    session_id = session.cookies.get_dict()['_session_id']
    soup = BeautifulSoup(response.text, features="html.parser")
    token = soup.find('meta', {'name': 'csrf-token'})['content']
    headers = {
        'Cookie': f'_session_id={session_id}',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Referer': 'https://pm.is74.ru/login',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 '
                      'Safari/537.36 '
    }
    data = {
        'utf-8': '%E2%9C%93',
        'authenticity_token': token,
        'username': username,
        'password': password,
        'login': 'Login'
    }
    session.post('https://pm.is74.ru/login', data=data, headers=headers)
    session_id = session.cookies.get_dict()['_session_id']

    return session_id


def is_pm_login():
    """
    Проверяет наличие куки авторизации в PM для парсинга активночти

    Returns:
        True or False
    """
    r = requests.get('https://pm.is74.ru/')
    pm_soup = BeautifulSoup(r.text, features="html.parser")
    if pm_soup.find('div', {'id': 'login-form'}) is None:
        return True
    else:
        return False


def get_current_pm_tasks(user_id, pm_id):
    """
    Парсит задачи в PM, назначенные на конкретного пользователя
    Args:
        user_id: идентификатор пользователя в БД
        pm_id: идентификатор пользователя в pm.is74.ru

    Returns:
        Записывает данные по задачам в таблицу БД (pm_tasks)
    """
    global session_id
    if not is_pm_login():
        session_id = pm_login()
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Cookie': f'_session_id={session_id}',
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 '
                      'Safari/537.36 '
    }
    req_git = requests.get(f'https://pm.is74.ru/issues.csv?assigned_to_id={pm_id}&key'
                           '=14af74365231aa54c1e90c16b1ce609a822f7cdf', headers=headers, allow_redirects=True)
    open('./tmp/user_tasks.csv', 'wb').write(req_git.content)
    try:
        df = pd.read_csv('./tmp/user_tasks.csv', sep=';')
        user_tasks = df.to_dict('records')
        for task in user_tasks:
            task_date = datetime.datetime.strptime(task['Обновлено'], '%d.%m.%Y %H:%M').strftime(
                '%Y-%m-%d %H:%M:%S')
            theme = task['Тема'].replace("'", "").replace('"', '')
            query = (f"replace into db.pm_tasks (task_id, user_id, project, tracker, status, name, update_time) "
                     f"values ({task['#']}, {user_id}, \'{task['Проект']}\', \'{task['Трекер']}\', \'{task['Статус']}\',"
                     f" \'{theme}\', \'{task_date}\')")
            mysql_worker(query)
    except:
        app.logger.error(f'Error while taking tasks for user {pm_id}')


def get_pm_report_for_user(user_id, pm_id):
    """
    Парсит дейтсвия пользователя в PM
    Args:
        user_id: идентификатор пользователя в БД
        pm_id: идентификатор пользователя в pm.is74.ru

    Returns:
        Записывает данные по задачам в таблицу БД (pm_actions)
    """
    req_git = requests.get(f'https://pm.is74.ru/activity.atom?key={CONFIG["parser"]["pm_key"]}&user_id={pm_id}')
    activity = json.loads(json.dumps(xmltodict.parse(req_git.text)))['feed']['entry']
    app.logger.info(f'Collecting {len(activity)} PM actions')
    tqdm_out = TqdmToLogger(app.logger, level=logging.INFO)
    for action in tqdm(activity, file=tqdm_out, mininterval=1, ):
        action_time = (dateutil.parser.parse(action['updated']) + datetime.timedelta(hours=5))
        if action_time.date() >= datetime.date.today():
            try:
                time_entries = ''
                if 'link' in action and '@href' in action['link'] and 'wiki' in action['link']['@href']:
                    date = (dateutil.parser.parse(action['updated']) + datetime.timedelta(hours=5)).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    title = description_handler(action['title'])
                    query = (f"replace into db.pm_wiki_actions (action_id, user_id, title, link, datetime) "
                             f"values (\"{date}\", {user_id}, \"{title}\", "
                             f"\"{unquote(action['link']['@href'])}\", \'{date}\')")
                    mysql_worker(query)
                    continue
                if 'Идея' in action['title'].split('#')[0]:
                    project_name = action['title'].split('#')[0].split(' - ')[0]
                    if 'час' in action['title']:
                        time_entries = action['title'].split(' - ')[1].split(' (')[0]
                elif not len(action['title'].split(' - Задача')) > 1:
                    if 'час' in action['title']:
                        project_name = action['title'].split(' - ')[0]
                        time_entries = action['title'].split(' - ')[1].split(' (')[0]
                    else:
                        project_name = action['title'].split(' - ')[0]
                else:
                    project_name = action['title'].split(' - Задача')[0]
                project_name = project_name.replace("'", "").replace('"', '')
                task = action['title'][action['title'].find("#") + 1:].split(':')[0]
                task_number = task.split('(')[0].replace(' ', '')
                task_status = task.split('(')[1].replace(')', '') if len(task.split(' ')) > 1 else ''
                if task_status != '':
                    if not '#text' in action['content'] and time_entries == '':
                        task_action = 'status_change'
                        query = f'UPDATE db.pm_tasks SET status = \"{task_status}\" WHERE task_id in (SELECT task_id FROM (SELECT * ' \
                                f'FROM db.pm_tasks) t where t.task_id = {task_number})'
                        mysql_worker(query)
                        description = f'{task_status}'.replace('"', '').replace("'", "")
                    elif '#text' in action['content'] and time_entries == '':
                        task_action = 'status_change__add_reply'
                        query = f'UPDATE db.pm_tasks SET status = \"{task_status}\" WHERE task_id in (SELECT task_id FROM (SELECT * ' \
                                f'FROM db.pm_tasks) t where t.task_id = {task_number})'
                        mysql_worker(query)
                        description = description_handler(action['content']['#text'])
                        description = (
                                f'Смена статуса на \'_{task_status}_\' и добавление комментария: \'' + description + '\'')
                    elif time_entries != '' and not '#text' in action['content']:
                        task_action = 'add_time_entries'
                        description = f'Оценка трудозатрат ({time_entries})'.replace('"', '').replace("'", "")
                    else:
                        task_action = 'add_time_entries__add_reply'
                        description = description_handler(action['content']['#text'])
                        description = (f'Оценка трудозатрат ({time_entries}) и добавление комментария: \'' + description
                                       + '\'')
                else:
                    task_action = 'add_reply'
                    description = description_handler(action['content']['#text'])
                req_git = requests.get(f'https://pm.is74.ru/issues/{task_number}.atom?'
                                       f'key=14af74365231aa54c1e90c16b1ce609a822f7cdf')
                try:
                    task_text = json.loads(json.dumps(xmltodict.parse(req_git.text)))['feed']
                    task_name = task_text['entry'][0]['title'].split(': ')[1]
                except Exception as e:
                    headers = {'Authorization': CONFIG['parser']['pm_auth_token']}
                    page = requests.get(f'https://pm.is74.ru/issues/{task_number}', headers=headers)
                    soup = BeautifulSoup(page.text, 'html.parser')
                    task_name = (soup.find('title').text).split(': ')[1].split(' - ')[0]

                task_name = task_name.replace("'", "").replace('"', '')
                date = (dateutil.parser.parse(action['updated']) + datetime.timedelta(hours=5)).strftime(
                    '%Y-%m-%d %H:%M:%S')
                query = (f"replace into db.pm_actions (task_id, user_id, task_number, task_name, project_name, "
                         f"action, description, datetime) values (\"{task_number}_{date}\", {user_id}, {task_number}, "
                         f"\"{task_name}\", \"{project_name}\", \"{task_action}\", \"{description}\", \'{date}\')")
                mysql_worker(query)
            except Exception as e:
                app.logger.error('Error in action')
                app.logger.error(e)
                app.logger.error(action)


def description_handler(desc):
    """
    Обработчик текста описания задачи. Удаляет лишние символы, ссылки, проверяет и ограничивает длину описания
    Args:
        desc: текст описания

    Returns:
        Обработанный текст описания
    """
    new_description = str(h.handle((desc).strip()).replace("_", "-").replace("*", "#").replace('`', ''))
    new_description = (re.sub("^\s+|^\n", '', new_description)).split('\n')[0]
    description = new_description if (len(new_description) <= 1000) \
        else new_description[:1000] + '...'
    description = description.replace('"', '').replace("'", "")

    return description


def get_git_report_for_user(user_id, username: str, interval: Tuple[date, date]) -> int:
    """
    Возвращает кол-во активностей для пользователя
    Args:
        username: логин пользователя
        interval: интервал, за который нужно собрать статистику

    Returns:
        кол-во активностей
    """
    user = {
        'merge_requests': 0,
        'commits': 0
    }
    tqdm_out = TqdmToLogger(app.logger, level=logging.INFO)
    headers = {'PRIVATE-TOKEN': CONFIG['parser']['git_token']}
    params = {'after': interval[0].date(), 'before': interval[1].date()}
    username = username.replace(' ', '').replace("'", "")
    req_git = requests.get(f'https://git.is74.ru/api/v4/users/{username}/events', headers=headers, params=params)
    answer = req_git.json()
    if answer != []:
        answer = answer[::-1]
        app.logger.info(f'Collecting {len(answer)} Git actions')
        for action in tqdm(answer, file=tqdm_out, mininterval=1, ):
            try:
                time = (dateutil.parser.parse(action['created_at']) + datetime.timedelta(hours=5)).strftime(
                    '%Y-%m-%d %H:%M:%S')
                if action['target_type'] == 'MergeRequest':
                    title = action['target_title'].replace('"', '').replace("'", "").replace('_', '-').replace('*', '')
                    query = (
                        f"replace into db.merge_requests (merge_id, user_id, title, datetime, action_name, project_id) "
                        f"values (\"{action['target_id']}_{user_id}\", {user_id}, \'{title}\', "
                        f"\'{time}\', \'{action['action_name']}\', {action['project_id']})")
                    mysql_worker(query)
                elif action['target_type'] == 'Issue':
                    req_issue = requests.get(f'https://git.is74.ru/api/v4/projects/{action["project_id"]}/issues/'
                                             f'{action["target_iid"]}', headers=headers)
                    issue = req_issue.json()
                    date = (dateutil.parser.parse(issue['updated_at'])).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    url = get_project_url(issue['project_id']) + f'/issues/{issue["iid"]}'
                    title = issue['title'].replace('"', '').replace("'", "").replace('*', '') \
                        .replace('_', '')
                    if issue['description']:
                        description = issue['description'].replace('"', '').replace("'", "").replace('*', '')\
                            .replace('_', '')
                    else:
                        description = ''
                    query = (
                        f"replace into db.issues (issue_id, project_id, user_id, action, title, description, url, datetime) "
                        f"values ({issue['id']}, {issue['project_id']}, {user_id}, \'{issue['state']}\', "
                        f"\'{title}\', \'{description}\', \'{url}\', \'{date}\')")
                    mysql_worker(query)
                elif 'push_data' in action:
                    if not 'Merge branch' in action['push_data']['commit_title']:
                        title = action['push_data']['commit_title'].replace('"', '').replace("'", "")
                        query = (
                            f"replace into db.pushes (push_id, user_id, commit_count, title, datetime, project_id, ref) "
                            f"values (\"{action['push_data']['commit_to']}_{time}\", {user_id}, "
                            f"{action['push_data']['commit_count']}, \"{title}\", \'{time}\', {action['project_id']}, "
                            f"\'{action['push_data']['ref']}\')")
                        mysql_worker(query)
                elif 'Note' in action['target_type'] and (action['target_type'] is not None):
                    description = description_handler(action['note']['body'])
                    resolvable = action['note']['resolvable']
                    if action['note']['noteable_type'] == 'MergeRequest':
                        url = get_project_url(
                            action['project_id']) + f'/merge_requests/{action["note"]["noteable_iid"]}' \
                                                    f'#note_{action["note"]["id"]}'
                    elif action['note']['noteable_type'] == 'Issue':
                        url = get_project_url(action['project_id']) + f'/issues/{action["note"]["noteable_iid"]}' \
                                                                      f'#note_{action["note"]["id"]}'
                    elif action['note']['noteable_type'] == 'Commit':
                        url = get_project_url(action['project_id'])
                    else:
                        url = ''
                    date = (dateutil.parser.parse(action['note']['updated_at'])).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    if resolvable:
                        resolved = action['note']['resolved']
                    else:
                        resolved = False
                    query = (
                        f"replace into db.notes (note_id, project_id, user_id, type, noteable_type, description,"
                        f" resolvable, resolved, url, datetime) "
                        f"values ({action['note']['id']}, {action['project_id']}, {user_id}, "
                        f"\'{action['target_type']}\', \'{action['note']['noteable_type']}\', \'{description}\',"
                        f" {resolvable}, {resolved}, \'{url}\', \'{date}\')")
                    mysql_worker(query)
            except Exception as e:
                app.logger.error(e)
                # app.logger.error(query)

        users[answer[0]['author']['username']] = user

    return len(answer)


def get_project_url(project_id):
    """
    Возвращает ссылку на проект в Giltab, или записывает его в БД, при отсутствии
    Args:
        project_id: идентификатор проекта

    Returns:
        Ссылка на проект в Gillab
    """
    query = (f'SELECT * FROM db.git_projects WHERE project_id={project_id}')
    result = mysql_worker(query, select=True)
    if result:
        for project in result:
            if project_id in project:
                return f'{project[2]}'
    else:
        headers = {'PRIVATE-TOKEN': CONFIG['parser']['git_token']}
        req_git = requests.get(f'https://git.is74.ru/api/v4/projects/{project_id}', headers=headers)
        url_answer = req_git.json()
        if 'name' in url_answer:
            query = (
                f"INSERT INTO db.git_projects (project_id, name, url) VALUES ({project_id}, \'{url_answer['name']}\', "
                f"\'{url_answer['web_url']}\')")
            mysql_worker(query)
            return get_project_url(project_id)
        else:
            return ''


def make_weekly_report(users: List[Tuple[str, str]]) -> str:
    """
    Генерирует недельный отчет по активности пользователей в гитлабе.

    Args:
        users: Список пар значений ("логин в телеге", "логин в гитлабе")

    Returns:
        текст отчета
    """
    result = str()
    date_end = datetime.datetime.now() + datetime.timedelta(days=1)
    date_start = date_end - datetime.timedelta(days=2)
    result_list = []
    for user in users:
        count_push = get_git_report_for_user(user[0], user[2], (date_start, date_end))
        get_pm_report_for_user(user[0], user[4])
        result_list.append((user[0], count_push))

    return result_list


def main():
    while True:
        app.logger.info('Start parsing PM and Git')
        query = (f"SELECT * from db.users")
        users = mysql_worker(query, True)
        try:
            make_weekly_report(users)
            app.logger.info('Successful parsing')
        except Exception as e:
            app.logger.error(e)
        sleep(60)


@app.route('/')
def hello():
    return 'hi'


@app.route('/tasks')
def tasks():
    tg_id = request.args.get('id')
    query = (f"SELECT * from db.users where tg_id={tg_id}")
    pm_users = mysql_worker(query, True)
    try:
        for user in pm_users:
            query = (f"DELETE from db.pm_tasks where user_id={user[0]}")
            mysql_worker(query)
            get_current_pm_tasks(user[0], user[4])
        app.logger.info('Successful task refreshing')
    except Exception as e:
        app.logger.error('Error while refresh user tasks')
        app.logger.error(e)

    return 'ok'


@app.route('/parse')
def parse():
    tg_id = request.args.get('id')
    if tg_id != 'all':
        query = (f"SELECT * from db.users where tg_id={tg_id}")
    else:
        query = (f"SELECT * from db.users")
    user = mysql_worker(query, True)
    try:
        make_weekly_report(user)
        app.logger.info('Successful parsing')
    except Exception as e:
        app.logger.error('Error while parsing user')
        app.logger.error(e)

    return 'ok'


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
