import yaml


def get_config(path: str):
    with open(path) as config_file:
        config = yaml.safe_load(config_file)
    return config


def set_state(path, key, state):
    with open(path) as f:
        doc = yaml.load(f)

    doc[key] = state

    with open(path, 'w') as f:
        yaml.dump(doc, f, default_flow_style=False, allow_unicode=True)