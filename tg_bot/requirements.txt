pytelegrambotapi==3.6.7
mysql-connector-python==8.0.19
requests==2.22.0
xmltodict==0.12.0
pyyaml==5.3
schedule==0.6.0
matplotlib==3.1.3
