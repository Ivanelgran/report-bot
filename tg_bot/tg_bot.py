"""
Входная точка для сервиса
"""
import multiprocessing
import time

import requests
import schedule

from bot_api.api import entry_point
from mysql.connector import connect
from bot_api.core import bot, mysql_worker, logger
from bot_api.skills.report_by_user import get_report_for_user
from bot_api.skills.tasks_report import tasks_reminder
from bot_api.skills.week_report import send_week_report_for_all_users
from common_lib import config

CONFIG = config.get_config('./common_lib/config/config.yml')


def force_report_handler():
    NEW_CONFIG = config.get_config('./common_lib/config/config.yml')
    if NEW_CONFIG['force_reporting']:
        query = (
            f"SELECT tg_id from users where user_id in (SELECT user_id from report_activity where is_report_sent='0')")
        user_info = mysql_worker(query, select=True)
        for tg_id in user_info:
            if int(tg_id[0]) not in [226007089, 127164083]:
                get_report_for_user(int(tg_id[0]), CONFIG['tg_bot']['chat_id'])
    refresh_report_activity()


def send_week_report():
    requests.get(f'http://stat_parser:5000/parse?id=all')
    week_report = send_week_report_for_all_users()
    bot.send_message(int(CONFIG['tg_bot']['chat_id']), 'Настало время подвести итоги недели:', parse_mode='Markdown')
    bot.send_message(int(CONFIG['tg_bot']['chat_id']), week_report, parse_mode='Markdown')
    bot.send_message(int(CONFIG['tg_bot']['chat_id']), 'Всем спасибо за продуктивную работу и хороших выходных :)',
                     parse_mode='Markdown')


def report():
    try:
        requests.get(f'http://stat_parser:5000/parse?id=all')
        query = (f"SELECT * from db.users")
        users = mysql_worker(query, select=True)
        for user in users:
            try:
                logger.info(f'Sending message to {user[3]}')
                get_report_for_user(user[1])
                tasks_reminder(user[1])
            except Exception as e:
                logger.error(f'Error while sending message to {user[3]}')
                logger.error(e)
    except Exception as e:
        logger.error(e)


def shedule_handler():
    while True:
        schedule.run_pending()
        time.sleep(1)


def refresh_tasks():
    requests.get(f'http://stat_parser:5000/parse?id=all')
    query = (f"SELECT * from db.users")
    users = mysql_worker(query, select=True)


def refresh_report_activity():
    query = (f"UPDATE db.report_activity SET is_report_sent = 0;")
    mysql_worker(query)


schedule.every(10).minutes.do(refresh_tasks)
schedule.every().monday.at('16:55').do(report)
schedule.every().day.at("23:50").do(force_report_handler)
schedule.every().tuesday.at('16:55').do(report)
schedule.every().wednesday.at('16:55').do(report)
schedule.every().thursday.at('16:55').do(report)
schedule.every().friday.at('16:55').do(report)
schedule.every().friday.at('17:00').do(send_week_report)

if __name__ == '__main__':
    p = multiprocessing.Process(target=shedule_handler)
    p.start()
    while True:
        try:
            bot.polling(none_stop=True, interval=0)
        except Exception as ex:
            logger.error(ex)
            time.sleep(10)
