from bot_api.core import mysql_worker, logger
import mysql


def insert_into_table(table, columns, values):
    try:
        query = (f"insert into {table} ({', '.join(columns)}) values ({', '.join(values)})")
        mysql_worker(query)

    except mysql.connector.Error as error:
        logger.error("Failed to insert record into table {}".format(error))
