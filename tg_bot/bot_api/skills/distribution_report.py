"""Скилл, отображающий график с распределением активностей
"""
import datetime
from math import pi

from matplotlib import pyplot as plt

from bot_api.core import DAYS_COUNT, mysql_worker, bot
from bot_api.skills.report_by_user import select_data
from common_lib import config

import bot_api.api as api
CONFIG = config.get_config('./common_lib/config/config.yml')


def show_activity_distribution(tg_id: int):
    """
    Формирует график по активностям пользователя и посылает его в ТГ
    Args:
        tg_id: Идентификатор пользователя в ТГ

    Returns:
        Послыает изображение графика в ТГ
    """
    date_end = datetime.datetime.now()
    date_start = (date_end - datetime.timedelta(days=DAYS_COUNT)).strftime('%d.%m.%Y %H:%M:%S')
    date_end = date_end.strftime('%d.%m.%Y %H:%M:%S')
    query = (f"SELECT SQL_NO_CACHE user_id from db.users where tg_id={tg_id}")
    user_id = mysql_worker(query, select=True)[0][0]

    pm_activity = select_data(user_id, 'pm_actions')
    merge_requests_activity = select_data(user_id, 'merge_requests')
    pushes_activity = select_data(user_id, 'pushes')
    issues_activity = select_data(user_id, 'issues')
    notes_activity = select_data(user_id, 'notes')
    activities = [pm_activity, merge_requests_activity, pushes_activity]
    names = ['PM', 'MergeRequest', 'Commit&Push']
    if issues_activity:
        names.append('Issues')
        activities.append(issues_activity)
    elif notes_activity:
        names.append('Review')
        activities.append(notes_activity)
    values = [len(i) for i in activities]
    plot_count = len(names)
    line_width = 3

    fig = plt.figure(figsize=(5, 5), facecolor='white')
    ax = fig.add_subplot(111, polar=True)
    fig.patch.set_visible(False)
    angles = [n / float(plot_count) * 2 * pi for n in range(plot_count)]

    plt.yticks([], [])
    ax.xaxis.set_tick_params(width=line_width)
    plt.xticks(angles, names, color='gray', size=14)

    ax.set_rlabel_position(0)
    # ax.plot(angles + [angles[0]], values + [values[0]], linewidth=line_width, linestyle='solid')
    ax.scatter(angles, values, s=80, linewidths=3, marker='o', facecolors='white', edgecolors='#72aee5', zorder=10)
    for i, (value, text) in enumerate(zip(values, names)):
        if sum(values) == 0:
            bot.send_message(tg_id, 'Отсутсвуют данные для создания графика')
            return 0
        percentile_value = int(value / sum(values) * 100)
        xy = (i / float(plot_count) * 2 * pi, value)
        title = f'    {value} ({percentile_value} %)'
        ax.annotate(title, xy)
    ax.fill(angles, values, '#007bff', alpha=0.4)
    ax.spines['polar'].set_visible(False)

    fig.savefig('/tmp/tg_id.png', dpi=300)
    photo = open('/tmp/tg_id.png', 'rb')
    bot.send_photo(tg_id, photo)
