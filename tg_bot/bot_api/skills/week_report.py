import datetime
from math import ceil, floor

from bot_api.core import bot, mysql_worker, logger

tables = [
    'pm_actions', 'merge_requests', 'pushes', 'issues', 'notes'
]

cells_length = [
    14, 4, 4, 7, 5, 6
]


def send_week_report_for_all_users():
    """
    Формирование недельного отчета по пользователем в виде таблицы из символов

    Returns:
        Сформированная таблица
    """
    score_dict = {}
    users_dict = {}
    table_row = "```\n|--------------|----|----|-------|-----|------|\n" \
                "|      Имя     | PM | MR |Commits|Issue|Review|\n" \
                "|--------------|----|----|-------|-----|------|\n"
    query = (f"SELECT * from db.users")
    users = mysql_worker(query, select=True)
    for user in users:
        fullname = user[3].split(' ')
        # username = f'[{fullname[1]} {fullname[0][:1]}.](@{user[2]})'
        username = f'{fullname[1]} {fullname[0][:1]}.'
        action_count = []
        for table in tables:
            count = get_activity_count(table, user[0])
            action_count.append(count)
        score = sum(action_count)
        action_count.insert(0, username)
        users_dict[username] = cell_shaper(action_count)
        score_dict[username] = score
    score_dict = list(score_dict.items())
    score_dict.sort(key=lambda i: i[1], reverse=True)
    for user in score_dict:
        table_row += '|' + '|'.join(users_dict[user[0]]) + '|'
        table_row += '\n|--------------|----|----|-------|-----|------|\n'
    table_row += '\n```'
    return table_row


def cell_shaper(activity_counts):
    """
    Формирует ячейки для строки в таблице. Учитывает необходимую длину ячейки и отцентровывает значение активности
    в строке. Пример в случае, если длина ячейки должна быть равна 6:    |   9  |
    Args:
        activity_counts: Массив с суммой активностей по всем видам (PM, Issue, Commits...)

    Returns:
        Массив с ячейками для объединения в строку таблицы
    """
    activity_counts = [str(i) for i in activity_counts]
    new_activity = []
    for i in range(len(activity_counts)):
        if len(activity_counts[i]) < cells_length[i] or i == 0:
            if i == 0:
                activity = activity_counts[i]
            else:
                activity = activity_counts[i]
            row_diff = cells_length[i] - len(activity)
            if row_diff % 2 == 0:
                new_activity.append(' ' * int(row_diff / 2) + str(activity_counts[i]) + ' ' * int(row_diff / 2))
            else:
                new_activity.append(
                    ' ' * int(ceil(row_diff / 2)) + str(activity_counts[i]) + ' ' * int(floor(row_diff / 2)))
        elif len(activity_counts[i]) == cells_length[i]:
            new_activity.append(str(activity_counts[i]))
        else:
            new_activity.append(str(activity_counts[i])[:len(activity_counts[i])])

    return new_activity


def get_activity_count(table, user_id):
    """
    Подсчитывает сумму активностей за неделю по таблицам с действиями
    Args:
        table: Таблица для подсчета
        user_id: Идентификатор пользователя в БД

    Returns:
        Сумму по активностям в таблице за неделю
    """
    date_start, date_end = get_time_boundaries()
    query = f"select count(*) from db.{table} where (datetime between " \
            f"\'{date_start}\' and \'{date_end}\') and user_id ={user_id}"
    count = mysql_worker(query, select=True)[0][0]
    return count


def get_time_boundaries():
    """
    Формирует границы времени начала и конца текущей недели

    Returns:
        Дату начала и конца недели
    """
    date_end = datetime.datetime.now()
    date_start = (datetime.date.today() - datetime.timedelta(days=6)).strftime('%Y-%m-%d %H:%M:%S')
    date_end = date_end.strftime('%Y-%m-%d %H:%M:%S')
    return date_start, date_end
