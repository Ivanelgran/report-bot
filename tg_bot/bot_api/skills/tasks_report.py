from bot_api.core import bot, mysql_worker, DAYS_COUNT, logger
from bot_api.forismatic.manager import Forismatic
import requests
from datetime import datetime


def get_activity_report(tg_id: int):
    """
    Формирует и посылает отчет по последней активности пользователя по его задачам
    Args:
        tg_id: Идентификатор пользователя в ТГ

    Returns:
        Отчет по активности (Ссылка на задачу; заголовок; время, прошедшее с последней активности по задаче)
    """
    tasks = get_active_pm_tasks(tg_id)
    if tasks:
        row = '*Последняя активность задач*:\n\n'
        for status in tasks:
            row += f'*{status}*\n'
            for task in tasks[status]:
                text = task[4].replace('_', '-').replace('*', '')
                row += f'• [{task[0]}](https://pm.is74.ru/issues/{task[0]}):' \
                       f' {text} ({last_activity_tracking(int(task[0]))})\n'
            row += '\n'
        bot.send_message(tg_id, row, parse_mode='Markdown')
    else:
        bot.send_message(tg_id, 'У Вас отсутсвуют активные задачи')


def tasks_reminder(tg_id: int):
    """
    Проверяет, по каким задачам не было активности в течении последних 24 часов и высылает напоминание в ТГ
    вместе с ежедневным отчетом
    Args:
        tg_id: Идентификатор пользователя в ТГ

    Returns:
        Высылает отчет с перечислением задач с плохой активностью
    """
    requests.get(f'http://stat_parser:5000/tasks?id={tg_id}')
    tasks = get_active_pm_tasks(tg_id)
    alert_tasks = []
    if tasks:
        for status in tasks:
            if status == 'В работе':
                for task in tasks[status]:
                    if calculate_hour_value(int(task[0])) > 24 and calculate_hour_value(int(task[0])) != -1:
                        text = task[4].replace('_', '-').replace('*', '')
                        new_row = f'• [{task[0]}](https://pm.is74.ru/issues/{task[0]}):' \
                                  f' {text} ({last_activity_tracking(int(task[0]))})'
                        alert_tasks.append(new_row)
    if alert_tasks:
        row = 'У Вас есть *текущие задачи*, которые *давно* не обновлялись:\n\n'
        row += '\n'.join(alert_tasks)
        row += '\n\nПожалуйста, *добавьте комментарии* к задаче или *смените её статус*'
        bot.send_message(tg_id, row,  parse_mode='Markdown')
        f = Forismatic()
        q = f.get_quote()
        bot.send_message(tg_id, f'Мотивирующая цитата:\n\n{q.quote}\t_{q.author}_', parse_mode='Markdown')


def get_active_pm_tasks(tg_id: int):
    """
    Возвращает активные задачи пользователя
    Args:
        tg_id: Идентификатор пользователя в ТГ

    Returns:
        Активные задачи пользователя
    """
    query = (f"SELECT user_id from db.users where tg_id={tg_id}")
    user_id = mysql_worker(query, select=True)[0][0]
    query = (f"SELECT * from db.pm_tasks where user_id={user_id} and not (status='Решена' or status='Отклонена')")
    user_tasks = mysql_worker(query, select=True)
    if user_tasks:
        return group_tasks_by_status(user_tasks)
    else:
        return []


def last_activity_tracking(task_number):
    """
    Формирует строку значения последней активности по задаче
    Args:
        task_number: Номер задачи в PM

    Returns:
        Время последней активности
    """
    hour_value = calculate_hour_value(task_number)
    if hour_value != 256879:
        if hour_value == 0:
            return 'Меньше часа назад'
        else:
            return get_correct_hour_case(hour_value) + ' назад'
    else:
        return 'Без активности'


def calculate_hour_value(task_number):
    """
    Возвращает количество часов, прошедших с последней активности по задаче
    Args:
        task_number: Номер задачи в PM

    Returns:
        Количество часов
    """
    query = (f"SELECT * from db.pm_actions where task_number={task_number} order by datetime desc limit 1")
    tasks = mysql_worker(query, select=True)
    if tasks:
        hour_value = round((datetime.now() - tasks[0][6]).total_seconds() / 3600)
        return int(hour_value)
    else:
        return 256879


def get_correct_hour_case(num):
    """
    Формирует строку с корректным склонением слово "час"
    Args:
        num: Количество часов

    Returns:
        Строка с количество часов
    """
    if str(num)[-1] == '1' and num != 11:
        return str(num) + ' час'
    elif str(num)[-1] in ['2', '3', '4'] and num not in [12, 13, 14]:
        return str(num) + ' часа'
    else:
        return str(num) + ' часов'


def group_tasks_by_status(user_tasks):
    """
    Группирует задачи пользователя по статусу задачи
    Args:
        user_tasks: Список с задачами

    Returns:
        Сгрупированный список задач по статусам
    """
    status_dict = {}
    statuses = list(set([f[3] for f in user_tasks]))
    for status in statuses:
        new_tasks = []
        for task in user_tasks:
            if status in task:
                new_tasks.append(task)
        status_dict[status] = new_tasks

    return status_dict
