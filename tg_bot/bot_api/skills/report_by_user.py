"""
Модуль со скиллом для отчета по активности в redmine и gitlab.
"""
import datetime
import requests
import json
from bot_api.core import bot, mysql_worker, DAYS_COUNT, logger
import calendar
from mysql.connector import connect
from telebot import types
from common_lib import config

CONFIG = config.get_config('./common_lib/config/config.yml')
a = calendar.LocaleHTMLCalendar(locale='Russian_Russia')
hideBoard = types.ReplyKeyboardRemove()


def set_report_mode(tg_id):
    NEW_CONFIG = config.get_config('./common_lib/config/config.yml')
    if NEW_CONFIG['force_reporting']:
        keyboard = types.InlineKeyboardMarkup()
        callback_button = types.InlineKeyboardButton(
            text='Выключить массовую рассылку',
            callback_data='chat_force_reporting__false')
        keyboard.add(callback_button)
    else:
        keyboard = types.InlineKeyboardMarkup()
        callback_button = types.InlineKeyboardButton(
            text='Включить массовую рассылку',
            callback_data='chat_force_reporting__true')
        keyboard.add(callback_button)

    bot.send_message(tg_id, 'Изменить статус:', reply_markup=keyboard)


def select_data(user_id, table, count=''):
    """
    Формирует и выполняет select запросы в БД
    Args:
        user_id: Идентификатор пользователя в БД
        table: Название таблицы в БД
        count: Нужно ли подсчитывать count в select запросе

    Returns:
        Результат выполнения запроса
    """
    date_end = datetime.datetime.now()
    date_start = (datetime.date.today()).strftime('%Y-%m-%d %H:%M:%S')
    date_end = date_end.strftime('%Y-%m-%d %H:%M:%S')
    if count == '':
        query = (
            f"SELECT * from db.{table} where user_id={user_id} and (datetime between \'{date_start}\' "
            f"and \'{date_end}\') order by datetime asc")
    else:
        query = (
            f"SELECT sum({count}) from db.{table} where user_id={user_id} and (datetime between \'{date_start}\'"
            f" and \'{date_end}\') order by datetime asc")
    result = mysql_worker(query, select=True)

    return result


def set_initials(message):
    """
    Устанавливает или возвращает инициалы пользователя из БД
    Args:
        message: Тело сообщения ТГ

    Returns:
        Инициалы пользователя
    """
    if len(message.text) != 2:
        msg = bot.send_message(
            message.chat.id,
            'ДВЕ БУКВЫ!!!1!1!')
        bot.register_next_step_handler(msg, set_initials)
    else:
        query = (f"SELECT user_id from db.users where tg_id={message.chat.id}")
        user_id = mysql_worker(query, select=True)[0][0]
        query = (f"UPDATE db.users t SET t.initials = '{(message.text).upper()}' WHERE t.user_id = {user_id}")
        mysql_worker(query)
        get_report_for_user(message.chat.id)


def tag_creator(initials):
    """
    Формирует тег для отчета (Пример: #МИ30920 (#_Инициалы_номер недели в месяце_номер месяцы в году
    _последние две цифры года))
    Args:
        initials: Инициалы пользователя

    Returns:
        Сформированный тег
    """
    week_number = 0
    today = datetime.datetime.now().date()
    weeks_for_month = a.monthdatescalendar(today.year, today.month)
    if weeks_for_month[0][0].day != 1:
        weeks_for_month = weeks_for_month[1:]

    for i in range(len(weeks_for_month)):
        for week_date in weeks_for_month[i]:
            if week_date.day == today.day and week_date.month == today.month:
                week_number = i + 1
                month = today.month
    if today.month != 1:
        if today in a.monthdatescalendar(today.year, today.month - 1)[-1]:
            if a.monthdatescalendar(today.year, today.month - 1)[0][0] != 1:
                week_number = len(a.monthdatescalendar(today.year, today.month - 1)) - 1
            else:
                week_number = len(a.monthdatescalendar(today.year, today.month - 1))
            month = today.month - 1
    else:
        if today in a.monthdatescalendar(today.year - 1, 12)[-1]:
            if a.monthdatescalendar(today.year - 1, 12)[0][0] != 1:
                week_number = len(a.monthdatescalendar(today.year - 1, 12)) - 1
            else:
                week_number = len(a.monthdatescalendar(today.year - 1, 12))
            month = 12
    if len(str(month)) == 1:
        month = '0' + str(month)
    else:
        month = str(month)

    return '#' + initials + str(week_number) + month + str(today.year)[2:]


def choose_user(tg_id: int):
    """
    Выбор пользователя для вывода отчета в админ панели
    Args:
       tg_id: Идентификатор пользователя в ТГ

    Returns:
       Список пользователей бота
    """
    query = (f"SELECT * from db.users")
    users = mysql_worker(query, select=True)
    keyboard = types.InlineKeyboardMarkup()
    for user in users:
        callback_button = types.InlineKeyboardButton(
            text=user[3],
            callback_data=f'user_id_{user[1]}')
        keyboard.add(callback_button)
    bot.send_message(tg_id, 'Выберите пользователя:', reply_markup=keyboard)


def get_report_for_user(tg_id, send_id=None):
    """Отсылает отчет пользователю

    Arguments:
        tg_id {int} -- id пользователя в телеграме
    """
    # NOTE: поменять название (скорее не get, а send)
    # NOTE: может разбить на 2 функции? (сбор отчета и его рассылка)
    # NOTE: формирование отчета в виде строки тяжело читается
    if send_id is None:
        send_id = tg_id
    date_end = datetime.datetime.now()
    date_start = (datetime.date.today()).strftime('%d.%m.%Y %H:%M:%S')
    date_end = date_end.strftime('%d.%m.%Y %H:%M:%S')
    query = (f"SELECT initials from db.users where tg_id={tg_id}")
    initials = mysql_worker(query, select=True)[0][0]
    if initials is None or initials == '':
        msg = bot.send_message(
            send_id,
            'Хм, похоже что у тебя все еще не задан тег\n'
            'Введи две буквы (инициалы), которые будут отображаться в твоем теге')
        bot.register_next_step_handler(msg, set_initials)
    else:
        query = (f"SELECT user_id from db.users where tg_id={tg_id}")
        user_id = mysql_worker(query, select=True)[0][0]
        pm_actions = select_data(user_id, 'pm_actions')
        pm_wiki_actions = select_data(user_id, 'pm_wiki_actions')
        if not pm_actions:
            bot.send_message(send_id,
                             f"Отчет за {date_end.split(' ')[0]} по PM {tag_creator(initials)}:\n"
                             f"_Отсутствие активности_ :(",
                             parse_mode='Markdown')
        else:
            status_change_tasks = get_status_changes(pm_actions)
            pm_actions = group_tasks_by_number(pm_actions)
            row = ''
            for project in pm_actions:
                row += f'\nПроект *\'{project}\'*:\n----------------------------------\n'
                for key_number in pm_actions[project]:
                    if pm_actions[project][key_number]:
                        row += f'*Задача* [#{pm_actions[project][key_number][0][7]}]' \
                               f'(https://pm.is74.ru/issues/{pm_actions[project][key_number][0][7]}) *' \
                               f'\"{pm_actions[project][key_number][0][2]}\":*\n'
                        if key_number in status_change_tasks:
                            if status_change_tasks[key_number]:
                                if len(status_change_tasks[key_number]) > 1:
                                    status_change_tasks[key_number] = status_change_tasks[key_number][1:]
                                row += f' • Смена статуса: _\'{" -> ".join(status_change_tasks[key_number])}\'_\n'
                        for task in pm_actions[project][key_number]:
                            if 'status_change' != task[4]:
                                row += f' • {task[5].replace("*", "")}\n'
            if pm_wiki_actions:
                row += f'\n*Редактировани Wiki*:\n'
                row += '----------------------------------\n'
                for wiki_action in pm_wiki_actions:
                    row += f' • [{wiki_action[2]}]({wiki_action[3]})\n'
            bot.send_message(send_id,
                             f"Отчет за {date_end.split(' ')[0]} по PM {tag_creator(initials)}: \n{row}",
                             parse_mode='Markdown')
        merge_requests = select_data(user_id, 'merge_requests')
        pushes = select_data(user_id, 'pushes')
        issues = select_data(user_id, 'issues')
        notes = select_data(user_id, 'notes')
        row = ''
        if merge_requests:
            row += '\n*Merge Requests*: \n'
            for action in merge_requests:
                project_url = get_project_url(action[5])
                if action[4] == 'accepted':
                    row += f'• {project_url} (Подтверждён) \"{action[2].replace("*", "").replace("_", "")}\"\n'
                elif action[4] == 'opened':
                    row += f'• {project_url} (Открыт) \"{action[2].replace("*", "").replace("_", "")}\"\n'
                else:
                    row += f'• {project_url} (Отклонен) \"{action[2].replace("*", "").replace("_", "")}\"\n'
        if pushes:
            row += '\n*Pushes:* \n'
            count = select_data(user_id, 'pushes', 'commit_count')
            row += f'_Всего коммитов_: *{count[0][0]}*\n'
            new_pushes = group_pushes_by_id(pushes)
            for project_id in new_pushes:
                project_url = get_project_url(project_id)
                row += f'{project_url}:\n'
                for action in new_pushes[project_id]:
                    row += f'• \"{str(action[3]).replace("*", "").replace("_", "")}\" ' \
                           f'({action[2]}) в *{action[6]}*\n'
        if issues:
            row += '\n*Issues*: \n'
            for issue in issues:
                if issue[3] == 'closed':
                    state = 'Закрыт'
                else:
                    state = 'Открыт'
                if issue[5]:
                    if len(issue[5]) > 100:
                        row += f'• ({state}) [{issue[4]}]({issue[6]}): {issue[5][:100]}...\n'
                    else:
                        row += f'• ({state}) [{issue[4]}]({issue[6]}): {issue[5]}\n'
                else:
                    row += f'• ({state}) [{issue[4]}]({issue[6]})\n'
        if notes:
            row += '\n*Review*: \n'
            for note in notes:
                if len(note[5]) > 100:
                    description = f'{note[5][:100]}...'
                else:
                    description = note[5]
                if note[4] == 'MergeRequest':
                    target = 'MR'
                elif note[4] == 'Issue':
                    target = 'Issue'
                else:
                    target = 'Other'
                if note[3] == 'DiffNote':
                    target = 'Code'
                    if note[7]:
                        row += f'• [{target}]({note[8]})| (Решено) {description}\n'
                    else:
                        row += f'• [{target}]({note[8]})| (Не решено) {description}\n'
                else:
                    row += f'• [{target}]({note[8]})| {description}\n'
        if merge_requests or pushes or issues or notes:
            new_row = f"Отчет за {date_end.split(' ')[0]} по Git {tag_creator(initials)}: \n" + row
            bot.send_message(send_id, new_row, parse_mode='Markdown', reply_markup=hideBoard)


def group_pushes_by_id(pushes):
    """
    Группирует коммиты по проектам
    Args:
       pushes: Список с коммитами

    Returns:
        Сгруппированный по проектам список коммитов
    """
    pushes_dict = {}
    projects = list(set([f[5] for f in pushes]))
    for project in projects:
        new_pushes = []
        for push in pushes:
            if project in push:
                new_pushes.append(push)
        pushes_dict[project] = new_pushes

    return pushes_dict


def get_project_url(project_id):
    """
    Возвращает ссылку на проект в Giltab, или записывает его в БД, при отсутствии
    Args:
        project_id: идентификатор проекта

    Returns:
        Ссылка на проект в Gillab
    """
    query = (f'SELECT * FROM db.git_projects WHERE project_id={project_id}')
    result = mysql_worker(query, select=True)
    if result:
        for project in result:
            if project_id in project:
                return f'[{project[1]}]({project[2]})'
    else:
        headers = {'PRIVATE-TOKEN': CONFIG['parser']['git_token']}
        req_git = requests.get(f'https://git.is74.ru/api/v4/projects/{project_id}', headers=headers)
        answer = req_git.json()
        if 'name' in answer:
            query = (f"INSERT INTO db.git_projects (project_id, name, url) VALUES ({project_id}, \'{answer['name']}\', "
                     f"\'{answer['web_url']}\')")
            mysql_worker(query)
            return get_project_url(project_id)
        else:
            return ''


def group_tasks_by_number(pm_actions):
    """
    Группирует действия в PM по проектам
    Args:
       pm_actions: Список с коммитами

    Returns:
        Сгруппированный по проектам список действий
    """
    project_dict = {}
    indexes = list(set([d[7] for d in pm_actions]))
    projects = list(set([f[3] for f in pm_actions]))
    for project in projects:
        project_tasks = []
        for task in pm_actions:
            if project in task:
                project_tasks.append(task)
        project_dict[project] = project_tasks
    for project in project_dict:
        new_dict = {}
        for index in indexes:
            index_tasks = []
            for task in project_dict[project]:
                if index in task:
                    index_tasks.append(task)
            new_dict[index] = index_tasks
        project_dict[project] = new_dict

    return project_dict


def get_status_changes(pm_actions):
    """
    Фильтрует список действия в PM и оставляет только те, что связаны со сменой статуса задачи
    Args:
       pm_actions: Список с коммитами

    Returns:
        Список задач с измененным статусом
    """
    status_dict = {}
    indexes = list(set([d[7] for d in pm_actions]))
    for index in indexes:
        status_list = []
        for task in pm_actions:
            if index in task:
                if 'status_change' == task[4]:
                    status_list.append(task[5])
                status_dict[index] = status_list

    return status_dict
