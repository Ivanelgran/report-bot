"""
Модуль с основными скилами бота
"""
import requests
import json
from requests import RequestException
from bot_api.core import logger
from bot_api.core import CONFIG, logger, bot
from bot_api.skills.distribution_report import show_activity_distribution
from bot_api.skills.report_by_user import get_report_for_user, choose_user, set_report_mode
from bot_api.skills.tasks_report import get_activity_report
import bot_api.api as api
from config import set_state

skills_registry = [
    ('Развернутый отчет', 'long_report', get_report_for_user),
    ('График', 'distribution', show_activity_distribution),
    ('Активные задачи', 'active_tasks', get_activity_report)
]

admin_skills_registry = [
    ('Отчет по пользователю', 'choose_user', choose_user),
    ('Управление опубликованием отчетов', 'force_reporting', set_report_mode)
]


def refresh_data(task_type, tg_id):
    """
    Обновляет информацию по задачам PM для конкретного пользователя
    Args:
        task_type: parse (Активность по задачам), tasks (Задачи, назначенные на пользователя)
        tg_id: Идентификатор ТГ пользователя
    """
    try:
        requests.get(f'http://stat_parser:5000/{task_type}?id={tg_id}')
    except RequestException as e:
        logger.error(e)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    """
    Callback функция ТГ. Реагирует на нажатие кнопок пользователем в ТГ
    Args:
        call: Информация о вызове из ТГ
    """
    if call.message:
        if 'chat_force_reporting' in call.data:
            report_state = call.data.split('__')[1]
            set_state('./common_lib/config/config.yml', 'force_reporting', json.loads(report_state.lower()))
            bot.send_message(call.from_user.id, 'Успешная смена статуса')
            api.show_admin_skills(call.from_user.id)
        if 'user_id' in call.data:
            user_tg_id = call.data.split('_')[2]
            refresh_data('parse', int(user_tg_id))
            get_report_for_user(int(user_tg_id), call.from_user.id)
            api.show_admin_skills(call.from_user.id)
        for _, callback_data, func in admin_skills_registry:
            if call.data == callback_data:
                func(call.from_user.id)
        for _, callback_data, func in skills_registry:
            if call.data == callback_data:
                if callback_data != 'active_tasks':
                    refresh_data('parse', call.from_user.id)
                else:
                    refresh_data('parse', call.from_user.id)
                    refresh_data('tasks', call.from_user.id)
                try:
                    func(call.from_user.id)
                    api.show_skills(call.from_user.id)
                except Exception as e:
                    logger.error(e)
