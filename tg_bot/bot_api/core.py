"""Модуль с основным функционалом бота

TODO: рефакторинг (как вариант, разбить на подмодули)
"""
import logging
from pathlib import Path

from telebot import types
import telebot
import mysql.connector
from mysql.connector import Error
from mysql.connector.connection import MySQLConnection
from mysql.connector import pooling

from config import get_config

SRC_DIR = Path(__file__).resolve().parents[1]
user_dict = {}


class User:
    def __init__(self, tg_id):
        self.username = None
        self.full_name = None
        self.tg_id = tg_id
        self.git_username = None
        self.pm_id = None


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

DAYS_COUNT = 1
CONFIG = get_config(f'{SRC_DIR}/config/config.yml')

connection_pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="tg_parser_pool",
                                                              pool_size=5,
                                                              user=CONFIG['db']['user'],
                                                              password=CONFIG['db']['password'],
                                                              host=CONFIG['db']['host'],
                                                              database=CONFIG['db']['database'])


def mysql_worker(query, select=False):
    """
    Выполняет команды БД и возвращает результат
    Args:
        query: SQL строка для выполнения
        select: Если True - возвращает результат выполнения операции

    Returns:
        Результат выполнения операции
    """
    result = []
    try:
        connection_object = connection_pool.get_connection()
        if connection_object.is_connected():
            cursor = connection_object.cursor()
            cursor.execute(query)
            if select:
                result = cursor.fetchall()
            connection_object.commit()
    except Error as e:
        logger.info("Error while connecting to MySQL using Connection pool ", e)
        logger.info(query)
    finally:
        if (connection_object.is_connected()):
            cursor.close()
            connection_object.close()
    return result


git_headers = {'PRIVATE-TOKEN': f'{CONFIG["parser"]["git_token"]}'}
pm_headers = {'Authorization': f'{CONFIG["parser"]["pm_auth_token"]}'}
bot = telebot.TeleBot(f'{CONFIG["tg_bot"]["token"]}')
