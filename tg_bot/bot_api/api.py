"""
Модуль с основным API для бота
"""
from telebot import types

from bot_api.core import mysql_worker, bot, logger
from bot_api.handlers.git_info import get_git_id
from bot_api.skills.registry import skills_registry, admin_skills_registry
from config import get_config
from bot_api.skills.report_by_user import get_report_for_user


@bot.message_handler(commands=['admin'])
def admin_panel(message):
    """
    Входная точка админ панели
    """
    tg_id = message.from_user.id
    query = (f"SELECT * from db.users where tg_id={tg_id}")
    user_info = mysql_worker(query, select=True)
    is_admin = user_info[0][6]
    if is_admin:
        bot.send_message(tg_id, f"Здравствуйте, {user_info[0][3]}")
        show_admin_skills(tg_id)


@bot.message_handler(func=lambda message: message.forward_from is not None and message.forward_from.is_bot == True,
                     content_types=['text'])
def listen_for_report(message):
    """
    Слушает сообщения в чате и записывает, если пользователь отправил отчет из бота
    """
    query = (f"SELECT * from db.users where tg_id={message.from_user.id}")
    user_info = mysql_worker(query, select=True)
    query = (f"UPDATE db.report_activity t SET t.is_report_sent = 1 WHERE t.user_id = {user_info[0][0]}")
    try:
        mysql_worker(query)
        logger.info(f"User {user_info[0][3]} sent a report")
    except Exception as e:
        logger.error(f"Error writing report submission from user {user_info[0][3]}")
        logger.error(e)


@bot.message_handler(commands=['start'])
def entry_point(message):
    """
    Входная точка для бота.
    Любой новый чат будет начинаться с нее."""
    query = f"SELECT * from db.users where tg_id={message.from_user.id}"
    row = mysql_worker(query, select=True)
    user_id = message.from_user.id
    if row:
        show_skills(user_id)
    else:
        bot.send_message(message.from_user.id, 'Что то я тебя не знаю')
        msg = bot.send_message(message.from_user.id, 'Напиши свой username в git.is74.ru')
        bot.register_next_step_handler(msg, get_git_id)


def show_skills(user_id: int):
    """
    Выводит все зарегистрированные скиллы бота.
    Args:
        user_id: ид пользователя в телеграме
    """
    keyboard = types.InlineKeyboardMarkup()
    for skill_name, callback_data, _ in skills_registry:
        callback_button = types.InlineKeyboardButton(
            text=skill_name,
            callback_data=callback_data)
        keyboard.add(callback_button)
    bot.send_message(user_id, "К вашим услугам:", reply_markup=keyboard)


def show_admin_skills(tg_id):
    """
    Входная точка админ панели
    """
    keyboard = types.InlineKeyboardMarkup()
    for skill_name, callback_data, _ in admin_skills_registry:
        callback_button = types.InlineKeyboardButton(
            text=skill_name,
            callback_data=callback_data)
        keyboard.add(callback_button)
    bot.send_message(tg_id, 'К вашим услугам:', reply_markup=keyboard)
