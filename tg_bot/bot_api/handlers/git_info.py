"""Модуль с функциями для привязки gitlab и redmine аккаунтов

NOTE: Наименование модуля не самое удачное.
"""
import json
import requests

from telebot import types

from bot_api.core import user_dict, User, git_headers, bot, pm_headers
from bot_api.db_worker import insert_into_table
import bot_api.api as api


hideBoard = types.ReplyKeyboardRemove()
markup3 = types.ReplyKeyboardMarkup(one_time_keyboard=True)
markup2 = types.ReplyKeyboardMarkup(one_time_keyboard=True)
itembtn1 = types.KeyboardButton('Да')
itembtn2 = types.KeyboardButton('Нет')
itembtn3 = types.KeyboardButton('Не знаю')
markup3.row(itembtn1)
markup3.row(itembtn2)
markup3.row(itembtn3)
markup2.row(itembtn1)
markup2.row(itembtn2)


def get_git_id(message):
    """
    Обрабатывает ввод пользователем Git ID и верефицирует его
    Args:
        message: Тело сообщения ТГ
    """
    git_username = message.text.replace('@', '')
    chat_id = message.chat.id
    user = User(message.from_user.id)
    user.git_username = git_username
    user_dict[chat_id] = user
    git_user_info = json.loads(
        requests.get(
            f'https://git.is74.ru/api/v4//users?username={git_username}',
            headers=git_headers).text)
    if git_user_info:
        msg = bot.send_message(
            message.from_user.id,
            f"Стало быть Вы {git_user_info[0]['name']}?",
            reply_markup=markup3)
        user.full_name = git_user_info[0]['name']
        user_dict[chat_id] = user
        bot.register_next_step_handler(msg, git_answer_handler)
    else:
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
        markup.add('Да', 'Нет')
        msg = bot.send_message(
            message.from_user.id, 'Что то не могу найти тебя. Попробуем еще раз?',
            reply_markup=markup2)
        bot.register_next_step_handler(msg, git_try_again_handler)


def git_answer_handler(message):
    """
    Интсруктирует и обрабатывает ввод пользователем PM ID
    Args:
        message: Тело сообщения ТГ
    """
    answer = message.text
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if answer == 'Да':
        bot.send_message(
            message.from_user.id,
            'Отлично! Теперь давай узнаем твой ID в pm.is74.ru')
        bot.send_message(
            message.from_user.id,
            'Инструкция:\n\
            1. Переходите по ссылке pm.is74.ru/users/current.xml\n\
            2. Найдите 4-х значное число в поле <id>')
        msg = bot.send_message(
            message.from_user.id,
            'Пожалуйста, введите его ниже')
        bot.register_next_step_handler(msg, check_pm_id)
    elif answer == 'Нет':
        msg = bot.send_message(
            user.tg_id,
            'Окей, попробуем еще раз?',
            reply_markup=markup2)
        bot.register_next_step_handler(msg, git_try_again_handler)
    else:
        bot.send_message(
            message.from_user.id,
            'Серьёзно?? Не знаю?! Тебя на работу как вообще взяли то?!')
        msg = bot.send_message(
            message.from_user.id,
            'Так "Да" или "Нет", шутник?',
            reply_markup=markup2)
        bot.register_next_step_handler(msg, git_answer_handler)


def git_try_again_handler(message):
    """
    Обрабатывает согласие или несогласие пользователя на возобновления процесса регистрации Git
    Args:
        message: Тело сообщения ТГ
    """
    answer = message.text
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if answer == 'Да':
        msg = bot.send_message(user.tg_id, 'Напиши свой username в git.is74.ru')
        bot.register_next_step_handler(msg, get_git_id)
    else:
        bot.send_message(user.tg_id, 'Как скажешь', reply_markup=hideBoard)


def check_pm_id(message):
    """
    Обрабатывает ввод пользователем PM ID и верефицирует его, а затем заносит данные пользователя в БД
    Args:
        message: Тело сообщения ТГ
    """
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.pm_id = message.text
    try:
        pm_user = json.loads(
            requests.get(
                f'https://pm.is74.ru/users/{message.text}.json',
                headers=pm_headers).text)
        full_name = pm_user['user']['lastname'] + ' ' + pm_user['user']['firstname']
        if full_name == user.full_name:
            insert_into_table(
                'users',
                ['tg_id', 'git_username', 'full_name', 'pm_id'],
                [
                    str(user.tg_id), f"'{str(user.git_username)}'",
                    f"'{str(user.full_name)}'", str(user.pm_id)
                ])
            bot.send_message(
                user.tg_id,
                'Вы успешно зарегистрированы в системе!')
            api.show_skills(user.tg_id)
        else:
            msg = bot.send_message(
                user.tg_id,
                f'Хм, а тут Вас зовут по другому - {full_name}. Это тоже Вы?',
                reply_markup=markup3)
            bot.register_next_step_handler(msg, pm_answer_handler)
    except:
        msg = bot.send_message(
            message.from_user.id,
            'Что то не могу найти тебя. Попробуем еще раз?',
            reply_markup=markup2)
        bot.register_next_step_handler(msg, pm_try_again_handler)


def pm_answer_handler(message):
    """
    Обрабатывает согласие или несогласие пользователя на возобновления процесса регистрации PM при несовпадении имени
    в PM и Git
    Args:
        message: Тело сообщения ТГ
    """
    answer = message.text
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if answer == 'Да':
        insert_into_table(
            'users',
            ['tg_id', 'git_username', 'full_name', 'pm_id'],
            [str(user.tg_id), f"'{str(user.git_username)}'",
             f"'{str(user.full_name)}'", str(user.pm_id)])
        bot.send_message(user.tg_id, 'Вы успешно зарегистрированы в системе!')
        api.show_skills(user.tg_id)
    elif answer == 'Нет':
        msg = bot.send_message(user.tg_id, 'Окей, попробуем еще раз?', reply_markup=markup2)
        bot.register_next_step_handler(msg, pm_try_again_handler)


def pm_try_again_handler(message):
    """
    Обрабатывает согласие или несогласие пользователя на возобновления процесса регистрации PM
    Args:
        message: Тело сообщения ТГ
    """
    answer = message.text
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if answer == 'Да':
        msg = bot.send_message(user.tg_id, 'Напиши свой id в pm.is74.ru')
        bot.register_next_step_handler(msg, check_pm_id)
    else:
        bot.send_message(user.tg_id, 'Как скажешь', reply_markup=hideBoard)
